terraform {
  required_providers {
    cloudstack = {
      source  = "cloudstack/cloudstack"
      version = "0.4.0"
    }
  }
}

provider "cloudstack" {
  api_url = "https://sesi-cloud-ctl1.inria.fr/client/api"

  ## Provided by environment (Gitlab secrets)
  # api_key = "${var.cloudstack_api_key}"
  # secret_key = "${var.cloudstack_secret_key}"

  timeout = 1200
}

variable "REGISTRATION_TOKEN" {
  type = string
  sensitive = true
}

variable "SSH_PUBLIC_KEY" {
  type = string
}

variable "CI_USER_NAME" {
  type = string
}

resource "cloudstack_instance" "ubuntu" {
  ## It is a good practice to have the "{project name}-" prefix
  ## in VM names.
  name             = "gitlabcigallery-opentofu-ubuntu"
  service_offering = "Custom"
  template         = "ubuntu-20.04-lts"
  zone             = "zone-ci"
  details = {
    cpuNumber = 2
    memory    = 2048
  }
  expunge = true
  user_data = templatefile("cloud-init.yaml.tftpl", {
    REGISTRATION_TOKEN = var.REGISTRATION_TOKEN
    SSH_PUBLIC_KEY     = var.SSH_PUBLIC_KEY
  })
  tags = {
    bastion_user = var.CI_USER_NAME
  }
  connection {
    type                = "ssh"
    host                = self.name
    user                = "ci"
    private_key         = file("id_rsa")
    bastion_host        = "ci-ssh.inria.fr"
    bastion_user        = self.tags.bastion_user
    bastion_private_key = file("id_rsa")
  }
  provisioner "remote-exec" {
    when   = destroy
    inline = ["sudo gitlab-runner unregister --all-runners || true"]
  }
}

resource "cloudstack_instance" "windows" {
  ## It is a good practice to have the "{project name}-" prefix
  ## in VM names.
  name             = "gitlabcigallery-opentofu-windows"
  service_offering = "Custom"
  template         = "windows10-vs2022-runner"
  zone             = "zone-ci"
  details = {
    cpuNumber = 2
    memory    = 2048
  }
  expunge = true
  tags = {
    bastion_user = var.CI_USER_NAME
  }
  connection {
    type                = "ssh"
    host                = self.name
    user                = "ci"
    password            = "ci"
    bastion_host        = "ci-ssh.inria.fr"
    bastion_user        = self.tags.bastion_user
    bastion_private_key = file("id_rsa")
    target_platform     = "windows"
  }
  provisioner "remote-exec" {
    inline = [<<-EOF
      gitlab-runner start
      gitlab-runner register --non-interactive --tag-list opentofu,windows --executor shell --shell powershell --url https://gitlab.inria.fr --registration-token ${var.REGISTRATION_TOKEN}
      EOF
    ]
  }
  provisioner "remote-exec" {
    when   = destroy
    inline = ["gitlab-runner unregister --all-runners || true"]
  }
}

resource "cloudstack_instance" "macos" {
  ## It is a good practice to have the "{project name}-" prefix
  ## in VM names.
  name             = "gitlabcigallery-opentofu-macos"
  service_offering = "Custom"
  template         = "osx-15-runner"
  zone             = "zone-ci"
  details = {
    cpuNumber = 2
    memory    = 2048
  }
  expunge = true
  tags = {
    bastion_user = var.CI_USER_NAME
  }
  connection {
    type                = "ssh"
    host                = self.name
    user                = "ci"
    password            = "ci"
    bastion_host        = "ci-ssh.inria.fr"
    bastion_user        = self.tags.bastion_user
    bastion_private_key = file("id_rsa")
  }
  provisioner "remote-exec" {
    inline = [<<-EOF
      set -ex
      (
        export PATH=/usr/local/bin:$PATH
        gitlab-runner register --non-interactive --tag-list opentofu,macos --executor shell --url https://gitlab.inria.fr --registration-token ${var.REGISTRATION_TOKEN}
      ) >~/log.txt 2>&1
      EOF
    ]
  }
  provisioner "remote-exec" {
    when = destroy
    inline = [<<-EOF
      set -ex
      (
        export PATH=/usr/local/bin:$PATH
        gitlab-runner unregister --all-runners || true
      ) >~/log.txt 2>&1
      EOF
    ]
  }
}
