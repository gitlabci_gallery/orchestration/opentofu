# Using OpenTofu to specify a pool of virtual machines hosted on ci.inria.fr

## Why using OpenTofu to specify the pool of virtual machines used by continuous integration?

[OpenTofu](https://opentofu.org) is an open-source software to
specify declaratively the state of a cluster of virtual machines.

 > **Note:**
 > [OpenTofu](https://opentofu.org) is a fork based on the
 > last available MPL-licensed version of
 > [Terraform](https://www.terraform.io/), developed by HashiCorp and
 > that adopted a BSL license.

Virtual machines are specified by a collection of text-based `.tf`
files, that can be stored and versioned in the project repository
itself.

The arguments in favor of such an approach are similar to the
arguments in favor of having a versioned specification for CI/CD
pipelines (`.gitlab-ci.yml`, `Jenkinsfile`, etc.):

- having both cluster configuration and pipeline specification
  versioned in the project repository allows developer to keep track
  the history of the continuous integration environment: if something
  breaks, it is easier to see what has changed, in order to fix it or
  to roll back to a working version.

- the specification is a reliable part of the documentation of the
  continuous integration environment, and the history makes easier to
  follow its evolution, in particular in the case where the
  maintenance charge is shared among multiple developers.

- the specification makes the environment easily reproducible for
  other projects or other platforms, and common parts can be shared
  and factored. In particular, if the environment is lost (if virtual
  machines are accidentally destroyed), it can be automatically
  restored.

Moreover, the OpenTofu language provides us a convenient API to
interact with CloudStack (the platform behind ci.inria.fr), for
instance to create and destroy virtual machines on demand. This
particular point is illustrated in the project
[terraform-dynamic](https://gitlab.inria.fr/gitlabci_gallery/orchestration/terraform-dynamic).

This configuration language is documented
[on OpenTofu Language website section](https://opentofu.org/docs/language/).

## Prerequisites

In addition to CI/CD features and shared runners (see the
[Prerequisites section](https://gitlab.inria.fr/gitlabci_gallery#prerequisites)),
this project needs:

- Infrastructure to be activated:
  - On the left sidebar in the Gitlab interface, go to **Settings** → **General**.
  - Expand **Visibility, project features, permissions**.
  - Turn on **Infrastructure**.
  - Select Save changes.
  > **Notes:**
  > 1. Infrastructure as Code is described in the
  > [Gitlab documentation](https://docs.gitlab.com/ee/user/infrastructure/index.html#infrastructure-as-code)
  > 2. To view infrastructure status, on the left sidebar in the Gitlab
  > interface, go to **Operate** → **Terraform states**.

- A dedicated project on [ci.inria.fr](https://ci.inria.fr):
  see the
  [tutorial](https://ci.inria.fr/doc/page/web_portal_tutorial/)
  to create a new project.
  > **Notes:**
  > 1. OpenTofu will never touch on virtual machines that it
  > didn't create itself and these virtual machines will not prevent it
  > to create its own virtual machines, unless if there are naming
  > conflicts. Therefore, you don't need to start from an empty
  > project.
  > 2. [gitlabcigallery-opentofu](https://ci.inria.fr/project/gitlabcigallery-opentofu/show)
  > was created for this tutorial.

- A dedicated user on [ci.inria.fr](https://ci.inria.fr),
  which has the *Slave Admin* role on this project.
  > **Notes:**
  > 1. This user is recommended to be specific for this project
  > (this requires to use a specific email address, or a specific
  > suffix for mail accounts that support it).
  > 2. An email address gitlabci_gallery_user@inria.fr
  > was created on https://sympa.inria.fr and a
  > [CI user](https://ci.inria.fr/users/show/gitlabci_gallery_user@inria.fr)
  > was created with this email address for the purpose of this tutorial.

- This [CI user name](https://inria-ci.gitlabpages.inria.fr/doc/page/slaves_access_tutorial/#identify-your-user-name)
  should be added as variable `TF_VAR_CI_USER_NAME` of type
  **Variable** in CI/CD settings (cf.
  [details on how to set a CI/CD variable](https://gitlab.inria.fr/gitlabci_gallery#cicd-variables)).

  > **Notes:**
  > 1. The `TF_VAR_` prefix makes OpenTofu bind the value
  > `TF_VAR_MY_VARIABLE` to the `var.MY_VARIABLE` variable inside
  > OpenTofu configuration files
  > ([OpenTofu documentation](https://opentofu.org/docs/language/values/variables/#environment-variables)).
  > 2. `TF_VAR_CI_USER_NAME` is set to `ggal001` in our example.

- Cloudstack API and secret keys should be added as variables
  `CLOUDSTACK_API_KEY` and `CLOUDSTACK_SECRET_KEY` of type
  **Variable** in CI/CD settings.
  To get the Cloudstack API and secret keys:
  - Go to https://sesi-cloud-ctl1.inria.fr/
  - Login as the dedicated user, with `ci/<your-project-name>` as domain.
  - On the top-right of the page, click on your username, then select **Profile**.
  - Copy the fields **API Key** and **Secret key**.

- Gitlab runner registration token should be added as a variable
  `TF_VAR_REGISTRATION_TOKEN` of type **Variable** in CI/CD settings.
  This token will allow virtual machines deployed by OpenTofu to
  register themselves as GitLab runners to execute jobs in the
  project pipeline.
  To get the registration token:
  - On the left sidebar in the Gitlab interface, go to **Settings** → **CI/CD**.
  - Expand **Runners**.
  - In the **Project runners** section,
    click on the button with 3 points close to **New project runner**,
    it will open a **Registration token** window.
    There is a button **Copy token** just after the token to copy it
    to the clipboard.

  > **Notes:** 
  > In this example, the same project is used for hosting
  > the OpenTofu configuration files and the build steps that are
  > executed on the virtual machines maintained by OpenTofu, but
  > this is not necessary the case. A distinct project can be
  > dedicated to maintain the infrastructure by OpenTofu, with
  > a variable `REGISTRATION_TOKEN` that points to another project
  > dedicated for the build itself.

- This project needs a pair of passphrase-less SSH private/public keys
  for the GitLab shared runner to be able to connect to the deployed
  runners to unregister them from GitLab before deletion.
  You can use the following command to create a pair of SSH private/public keys
  without passphrase in the current directory (files `id_rsa` and `id_rsa.pub`):
  `ssh-keygen -b 4096 -f id_rsa -N ""`.
  - The contents of the private key file `id_rsa` should be added as a variable
    `SSH_PRIVATE_KEY` of type **File**  in CI/CD settings
    (cf.
    [details on how to set a CI/CD variable](https://gitlab.inria.fr/gitlabci_gallery#cicd-variables)).
  - The public key file `id_rsa.pub` should be registered on ci.inria.fr portal
    to allow the dedicated user to connect to the hosted virtual machines
    ([portal documentation](https://ci.inria.fr/doc/page/slaves_access_tutorial/#register-your-ssh-public-key)).
    for details on how to register a public key on the portal.
    The contents of the public key file should also be added as a variable
    `TF_VAR_SSH_PUBLIC_KEY` of type **Variable** in CI/CD settings:
    the OpenTofu configuration file [`main.tf`](main.tf) substitutes
    the public key in the cloud-init script template
    [`cloud-init.sh.tftpl`](cloud-init.sh.tftpl),
    to register the key in the file `~ci/.ssh/authorized_keys`
    in deployed virtual machines.

- The repository contains a `backend.tf` file for connecting
  OpenTofu with GitLab.
  This may be convenient to delete it locally for running `tofu`
  directly on the development machine to experiment before committing
  changes to Gitlab: in these settings, we prefer to use the default
  (local) backend.
  However, we do not want `git` to keep track of this deletion:
  we can run locally `git update-index --assume-unchanged backend.tf`
  after having removed `backend.tf` for `git` to ignore this change.

- The pipeline defined in `.gitlab-ci.yml` contains a step `format and validate`
  to check that `*.tf` files are formatted accordingly to the OpenTofu
  guidelines.
  Automatic reformatting and `*.tf` files validation can be performed locally
  respectively with the commands `tofu fmt` and  `tofu init; tofu validate`.
  The file [`.pre-commit-config.yaml`](.pre-commit-config.yaml)
  defines a pre-commit hook to validate configuration files and perform
  automatic reformatting before each commit: you may enable it by
  installing [pre-commit](https://pre-commit.com/#install) and initializing
  your repository with the command `pre-commit install`.
  It requires OpenTofu to be installed locally.

## The OpenTofu configuration file [`main.tf`](main.tf)

The configuration file [`main.tf`](main.tf) contains some
sections to set up CloudStack as a resource provider, and then the
specification of the resources themselves.

As explained in the previous section,
the secret `REGISTRATION_TOKEN` is passed through a variable.
```terraform
variable "REGISTRATION_TOKEN" {
  type      = string
  sensitive = true
}
```
The variable is marked as sensitive to prevent OpenTofu from showing its value in output
([OpenTofu documentation](https://opentofu.org/docs/language/values/variables/#suppressing-values-in-cli-output)). 

The SSH public key is passed through the variable `SSH_PUBLIC_KEY`.
```terraform
variable "SSH_PUBLIC_KEY" {
  type = string
}
```
The value of the `SSH_PUBLIC_KEY` variable will be stored in the file
`~ci/.ssh/authorized_keys` in virtual machines, so that OpenTofu can
connect to the virtual machines with the private key to unregister the
runners before destroying the machines.

In this example, we set up three resources:
 * a virtual machine running on Ubuntu 20.04
 * a virtual machine running on Windows 10
 * a virtual machine running on Mac OS X 15

The three virtual machines register themselves as runners on gitlab.inria.fr:
the Ubuntu machine provides a `docker` executor,
Windows and Mac OS X provide `shell` executors
(`powershell` for Windows, `bash` for Mac OS X).

### Ubuntu 20.04 virtual machine

```terraform
resource "cloudstack_instance" "ubuntu" {
  ## It is a good practice to have the "{project name}-" prefix
  ## in VM names.
  name             = "gitlabcigallery-opentofu-ubuntu"
  service_offering = "Custom"
  template         = "ubuntu-20.04-lts"
  zone             = "zone-ci"
  details = {
    cpuNumber = 2
    memory    = 2048
  }
  expunge = true
  user_data = templatefile("cloud-init.yaml.tftpl", {
    REGISTRATION_TOKEN = var.REGISTRATION_TOKEN
    SSH_PUBLIC_KEY     = var.SSH_PUBLIC_KEY
  })
  tags = {
    bastion_user = var.CI_USER_NAME
  }
  connection {
    type                = "ssh"
    host                = self.name
    user                = "ci"
    private_key         = file("id_rsa")
    bastion_host        = "ci-ssh.inria.fr"
    bastion_user        = self.tags.bastion_user
    bastion_private_key = file("id_rsa")
  }
  provisioner "remote-exec" {
    when   = destroy
    inline = ["sudo gitlab-runner unregister --all-runners || true"]
  }
}
```

- `cloudstack_instance` is an identifier for the resource, which can be
  used to refer to it elsewhere in the OpenTofu configuration;
- `gitlabcigallery-opentofu-ubuntu` is the name of the
  virtual machine: by convention, the prefix `gitlabcigallery-opentofu`
  is the name of the project on ci.inria.fr.
- The service offering `Custom` allows us to specify the characterics
  of the virtual machine in the details `section`:
  - `cpuNumber` should be between `1` and `16` (cores),
  - `memory` should be between `1024` and `24576` (GB).
- `template` can refer to a template by name or ID.
  The available templates can be listed with the ci.inria.fr portal in the
  virtual machine creation form
  ([portal documentation](https://ci.inria.fr/doc/page/web_portal_tutorial/#slaves)).
  We rely here on the fact that [`cloud-init`](https://cloud-init.io/)
  is installed in the template and takes into account the CloudStack user-data
  ([CloudStack documentation for cloud-init support](https://docs.cloudstack.apache.org/projects/cloudstack-administration/en/latest/virtual_machines.html#user-data-and-meta-data)).
  We could also use a `remote-exec` provisioner to connect the virtual machine
  via SSH to execute an initialization script on first boot: we will use
  this method with Windows and Mac OS X virtual machines, since `cloud-init`
  only exists on Linux.
- There is only one zone, `zone-ci`, and `expunge` should be set to
  `true` to ask CloudStack to destroy the virtual machine immediately
  when OpenTofu needs to replace it (by default, virtual machines are
  kept during 24h after deletion, which prevents OpenTofu for recreating
  a machine with the same name).
- `user_data` contains a script which is passed to
  [`cloud-init`](https://cloud-init.io/)
  to be run at the first boot of the virtual machine. The `templatefile`
  is used to read the cloud-init configuration file from file
  [`cloud-init.yaml.tftpl`](cloud-init.yaml.tftpl)
  by substituting `${REGISTRATION_TOKEN}` with the value of the variable
  passed to OpenTofu.
- We pass also the `SSH_PUBLIC_KEY` to the template file to have its
  value written in the `~ci/.ssh/authorized_keys` file.
  We configure the connection via ssh to the runner: `ggal001` is the
  login of the dedicated user on ci.inria.fr, and we will make sure
  in the next section that the private key is written in the file
  `id_rsa`.
  We cannot use a variable for passing the path to this file,
  since the connection is used by a destroy provisioner, that
  cannot refer to variables.
  This destroy provisioner executes `gitlab-runner unregister`
  before the destruction of the virtual machine; failures are ignored
  in case of the `gitlab-runner` command was not yet installed
  when destroying occurs.

### Windows 10 virtual machine

```terraform
resource "cloudstack_instance" "windows" {
  ## It is a good practice to have the "{project name}-" prefix
  ## in VM names.
  name             = "gitlabcigallery-opentofu-windows"
  service_offering = "Custom"
  template         = "windows10-vs2022-runner"
  zone             = "zone-ci"
  details = {
    cpuNumber = 2
    memory    = 2048
  }
  expunge = true
  tags = {
    bastion_user = var.CI_USER_NAME
  }
  connection {
    type                = "ssh"
    host                = self.name
    user                = "ci"
    password            = "ci"
    bastion_host        = "ci-ssh.inria.fr"
    bastion_user        = self.tags.bastion_user
    bastion_private_key = file("id_rsa")
    target_platform     = "windows"
  }
  provisioner "remote-exec" {
    inline = [<<-EOF
      gitlab-runner start
      gitlab-runner register --non-interactive --tag-list opentofu,windows --executor shell --shell powershell --url https://gitlab.inria.fr --registration-token ${var.REGISTRATION_TOKEN}
      EOF
    ]
  }
  provisioner "remote-exec" {
    when   = destroy
    inline = ["gitlab-runner unregister --all-runners || true"]
  }
}
```

- It is essential to specify `target_platform = "windows"` for the SSH
  connection to work.

- `gitlab-runner` service is not started on boot by default in the
  template: we start the service explicitly before registering the runner.

### Mac OS X 15 virtual machine

```terraform
resource "cloudstack_instance" "macos" {
  ## It is a good practice to have the "{project name}-" prefix
  ## in VM names.
  name             = "gitlabcigallery-opentofu-macos"
  service_offering = "Custom"
  template         = "osx-15-runner"
  zone             = "zone-ci"
  details = {
    cpuNumber = 2
    memory    = 2048
  }
  expunge = true
  tags = {
    bastion_user = var.CI_USER_NAME
  }
  connection {
    type                = "ssh"
    host                = self.name
    user                = "ci"
    password            = "ci"
    bastion_host        = "ci-ssh.inria.fr"
    bastion_user        = self.tags.bastion_user
    bastion_private_key = file("id_rsa")
  }
  provisioner "remote-exec" {
    inline = [<<-EOF
      set -ex
      (
        export PATH=/usr/local/bin:$PATH
        gitlab-runner register --non-interactive --tag-list opentofu,macos --executor shell --url https://gitlab.inria.fr --registration-token ${var.REGISTRATION_TOKEN}
      ) >~/log.txt 2>&1
      EOF
    ]
  }
  provisioner "remote-exec" {
    when = destroy
    inline = [<<-EOF
      set -ex
      (
        export PATH=/usr/local/bin:$PATH
        gitlab-runner unregister --all-runners || true
      ) >~/log.txt 2>&1
      EOF
    ]
  }
}
```

- In the `remote-exec` provisioner, outputs are redirected to `~/log.txt` to ease
  debugging, since they are not shown in GitLab log.

- The executable `gitlab-runner` is in `/usr/local/bin`, which is added to `PATH`
  by `.bashrc` (or `.zshrc`), which is not sourced when commands are executed via SSH
  non interactively.
  Therefore, we add `/usr/local/bin` specifically to `PATH` before executing
  `gitlab-runner` (we could have sourced `. ~/.bashrc` instead).

## The cloud-init configuration file [`cloud-init.yaml.tftpl`](cloud-init.yaml.tftpl)

The cloud-init configuration file
[`cloud-init.yaml.tftpl`](cloud-init.yaml.tftpl) sets up the following:
- the user `ci` can execute `sudo` without password, so that the destroy
provisioner be able to unregister the runners;
- the SSH public key is registered as authorized key for `ci`,
- by default, password authentication is disabled for `ci`
  (you may add `lock_passwd: false` to enable it again,
  [documentation](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#users-and-groups));
- `gitlab-runner` and `docker.io` is installed on the virtual machine,
  the runner is registered on gitlab.inria.fr.
The configuration file should begin with the following line.
```yaml
#cloud-config
```
You may provide a shell script with the according shebang (`#!/bin/sh`) instead.

## The pipeline specification file [`.gitlab-ci.yml`](.gitlab-ci.yml)

The pipeline specification file [`.gitlab-ci.yml`](.gitlab-ci.yml)
relies on the template
[`Terraform/Base.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Terraform/Base.gitlab-ci.yml),
provided by gitlab.com.
The usage of this template is described in the [Gitlab documentation](https://docs.gitlab.com/ee/user/infrastructure/iac/).

This template uses a Docker container
`$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.1:v0.43.0`, where
`CI_TEMPLATE_REGISTRY_HOST` is by default set to `registry.gitlab.com`:
to use it on shared runners, we have mirrored this Docker container locally to circumvent
quotas.
The container is available `registry.gitlab.inria.fr`
(that we use for `CI_TEMPLATE_REGISTRY_HOST`),
in the project `gitlab-org/terraform-images`.

There are five stages:
```yaml
stages:
  - static-analysis
  - build
  - deploy
  - execute
  - cleanup
```

- In the stage `static-analysis`, the step `format and validate`
  checks that there is no error in `*.tf` files and that they are
  properly formatted according to the guidelines.

- In the stage `build`, the step `build execution plan` plans the
  modifications to apply to CloudStack to conform the configuration
  file. The plan will be stored in cache.

- In the stage `deploy`, the homonymous step applies the plan.

- In the stage `execute`, the homonymous step executes a command using
  the docker-based GitLab runner hosted in the deployed virtual machine.

  > **Note:**
  > we chose in this example to perform the `execute` stage in the
  > same project, but we could have chosen to register the runner to
  > another project by adjusting the `REGISTRATION_TOKEN` variable.

- In the stage `cleanup`, the step `destroy` is to be run manually and
  executes `tofu destroy -auto-approve`,
  which destroys the virtual machines (and these virtual machines have
  `remote-exec` destroy provisioners that unregister themselves from
  gitlab.inria.fr).

Every job that will use the OpenTofu configuration file needs to copy
the file referred by `SSH_PRIVATE_KEY` into the file `id_rsa`.
To copy the file without overriding the script inherited from the
OpenTofu template, we use the `before_script` key:
we define a template job `.with-rsa-key`, which every job requiring
the `id_rsa` file will extend
(if there were no `execute` jobs, we could also have defined the
`before_script` key at top-level, outside any job,
making the file be copied before every job).

```yaml
.with-rsa-key:
  before_script:
    - cp $SSH_PRIVATE_KEY id_rsa
```

Every job that will use the OpenTofu configuration file also needs
`tofu` command to be installed. We define a template job `.opentofu`,
which every job requiring the `tofu` command will extend and that
is using the
[official OpenTofu docker image](https://github.com/opentofu/opentofu/pkgs/container/opentofu/versions?filters%5Bversion_type%5D=tagged)

```yaml
.opentofu:
  image: ghcr.io/opentofu/opentofu:1.6.1
  tags:
    - linux
    - small
```

In the `execute` stage, we compile `hello_world.c` with `gcc`
on Ubuntu, `cl` (Microsoft Visual Studio) on Windows,
and `clang` on Mac OS X.
On Windows, we need to run `vcvars64.bat` to get the
environment initialized for Visual Studio.
This batch file requires CMD shell, whereas `gitlab-runner`
deprecates CMD in favor of PowerShell (see
[documentation](https://docs.gitlab.com/runner/shells/)).
The documentation gives the following
[trick](https://gitlab.com/guided-explorations/microsoft/windows/call-cmd-from-powershell/-/blob/master/.gitlab-ci.yml) to execute batch files with CMD from PowerShell:
write the script in a file and run `CMD.EXE /C` on it.

```yaml
execute windows:
  stage: execute
  image: alpine
  tags:
    - opentofu
    - windows
  script:
    # The trick for running cmd scripts from powershell runner is documented
    # here:
    # https://gitlab.com/guided-explorations/microsoft/windows/call-cmd-from-powershell/-/blob/master/.gitlab-ci.yml
    - |
      set-content $env:public\inline.cmd -Value @'
      call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvars64.bat"
      cl /Fe:hello_world.exe hello_world.c
      '@
      CMD.EXE /C $env:public\inline.cmd
      exit $LASTEXITCODE
  artifacts:
    paths:
      - hello_world.exe
```

## Ignored files in [`.gitignore`](.gitignore)

[`.gitignore`](.gitignore) instructs git to ignore the local files
generated by the `tofu` command, in the case this command is used
locally for experimentation.
